# devsutest



## Running

You can build the docker image using

`sudo ./build.sh`

Run the docker image with

`sudo ./run.sh`

Run the two replicas with the load balancer using

`sudo sudo docker-compose up --scale hello-world=2`
